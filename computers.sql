

--
-- Table structure for table `computers`
--

CREATE TABLE `computers` (
  `id` int(32) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `timein` varchar(32) NOT NULL,
  `datein` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `cpu` varchar(64) NOT NULL,
  `cooler` varchar(64) NOT NULL,
  `motherboard` varchar(64) NOT NULL,
  `ram` varchar(64) NOT NULL,
  `storage` varchar(64) NOT NULL,
  `video` varchar(64) NOT NULL,
  `power` varchar(64) NOT NULL,
  `notes` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

